Dr. Luke Fullenkamp and our entire Cincinnati optometry team are committed to providing advanced vision care in a professional and comfortable environment. As an optometrist in Cincinnati, our primary eye care service includes a complete eye exam that analyzes eye health and vision function.

Address: 11711 Princeton Pike, #941, Cincinnati, OH 45246, USA

Phone: 513-671-0933
